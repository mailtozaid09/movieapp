import React, { useEffect } from 'react';
import { Text, View, LogBox, StatusBar, SafeAreaView, Image } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import SplashScreen from 'react-native-splash-screen';

import Navigator from './src/navigator';



import { Provider, useSelector, } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './src/store';
import MainNavigator from './src/navigator/MainNavigator';

LogBox.ignoreAllLogs(true);

const App = () => {

    useEffect(() => {
        setTimeout(() => {
            SplashScreen.hide()
        }, 1000);
    }, [])
    
    return (
        <Provider store={store}>
            <PersistGate persistor={persistor}>
                <NavigationContainer>
                    <StatusBar backgroundColor = "#0D0D0D"   />  
                    <MainNavigator />
                </NavigationContainer>
            </PersistGate>
        </Provider>
    )
}

export default App
