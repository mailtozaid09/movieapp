export const media = {
    
    logo: require('../assets/logo.png'),
    
    home: require('../assets/home.png'),
    home_fill: require('../assets/home_fill.png'),
    admin: require('../assets/admin.png'),
    admin_fill: require('../assets/admin_fill.png'),


    login: require('../assets/login.png'),
    
    
    hide: require('../assets/hide.png'),
    view: require('../assets/view.png'),
    close: require('../assets/close.png'),
    search: require('../assets/search.png'),

    heart_black: require('../assets/heart_black.png'),
    heart_white: require('../assets/heart_white.png'),
    heart_fill: require('../assets/heart_fill.png'),
    heart_outline: require('../assets/heart_outline.png'),

}
