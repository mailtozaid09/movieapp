export const colors = {
    primary: '#8F68C7',
    dark_primary: '#473f83',

    
    bg_color: '#F7F7F7',
    dark_gray: '#707070',

    light_red: '#FEA7A9',

    dark_blue: '#212c46',
    
    black: '#000',
    white: '#fdfdfd',
    
    gray: '#919399',
    light_gray: '#ebecf0',
    
    blue: '#81b3f3',
    red: '#f29999',
    orange: '#f7c191',
    reddish: '#ED4545',
}