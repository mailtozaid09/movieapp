import React, { useState, useEffect } from 'react'
import { Text, View, SafeAreaView, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, FlatList, Modal, TextInput, } from 'react-native';
import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';
import { media } from '../../global/media';
import { useNavigation } from '@react-navigation/native';

const Input = ({label, value, placeholder, keyboradType, onChangeText, showEyeIcon, onChangeEyeIcon, isPassword, error }) => {

    const navigation = useNavigation()

    return (
        <View style={{width: '100%'}} >
            <Text style={styles.heading} >{label}</Text>
            <View style={styles.inputContainer}>
                <TextInput
                    value={value}
                    placeholder={placeholder}
                    keyboardType={keyboradType}
                    maxLength={keyboradType == 'numeric' ? 10 : null}
                    secureTextEntry={isPassword  && !showEyeIcon ? true : false}
                    style={styles.inputStyle}
                    onChangeText={(text) => onChangeText(text)}
                />
                {isPassword && (
                    <TouchableOpacity onPress={onChangeEyeIcon}>
                        <Image source={showEyeIcon ? media.view : media.hide} style={styles.iconImage} />
                    </TouchableOpacity>
                )}
            </View>
            {error && <Text style={styles.error} >{error}</Text>}
        </View>
    ) 
}

const styles = StyleSheet.create({
    heading: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.black,
        marginTop: 10,
    },
    inputContainer: {
        borderWidth: 1,
        borderRadius: 10,
        marginTop: 4,
        width: '100%',
        borderColor: colors.black,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 12
    },
    inputStyle: {
        paddingVertical: 10,
        fontSize: 14,
        alignItems: 'center',
        color: colors.black,
        flex: 1,
        marginRight: 15,
    },
    iconImage: {
        height: 24,
        width: 24
    },
    error: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.reddish,
        marginTop: 2,
    }
})

export default Input