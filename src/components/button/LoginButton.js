import React from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import { media } from '../../global/media'
  
const LoginButton = ({ title, onPress, isOutlined, skipButton, disabled, icon }) => {
    return (
        <>
            <TouchableOpacity 
                activeOpacity={0.5}
                onPress={onPress}
                disabled={disabled}
                style={isOutlined ? styles.outlinedContainer : [styles.container, {backgroundColor: disabled ? '#d3d3d390' : colors.primary,}]} >
                <Text style={[styles.title, isOutlined ? {color: colors.primary} : {}]} >{title}</Text>
            </TouchableOpacity>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: 50,
        borderRadius: 10,
        marginVertical: 30,
        marginBottom: 0,
    },
    outlinedContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: 50,
        borderRadius: 10,
        borderColor: colors.primary,
        borderWidth: 1.5,
        marginVertical: 14,
    },
    title: {
        fontSize: fontSize.SubTitle,
        fontWeight: 'bold',
        textAlign: 'center',
        color: colors.white,
    },

})

export default LoginButton
