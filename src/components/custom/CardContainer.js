import React, { useState, useEffect } from 'react'
import { Text, View, SafeAreaView, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, FlatList, Modal, TextInput, } from 'react-native';
import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';
import { media } from '../../global/media';
import { useNavigation } from '@react-navigation/native';


const baseUrl = 'https://image.tmdb.org/t/p/w500/'

const CardContainer = ({ title, data }) => {

    const navigation = useNavigation()


    return (
        <View style={{width: '100%'}} >
            <Text style={styles.title} >{title}</Text>
            
            <FlatList
                data={data}
                horizontal
                showsHorizontalScrollIndicator={false}
                keyExtractor={item => item.id}
                renderItem={({item, index}) => (
                    <TouchableOpacity 
                        key={index} 
                        style={styles.cardContainer} 
                        onPress={() => {navigation.navigate('Details', {details: item})}}
                    >
                        
                        <Image source={{uri: `${baseUrl}${item.poster_path}`}} style={styles.iconImage} />
                        <View style={{position: 'absolute', top: 12, width: '100%'}} >
                            <View style={{ alignItems: 'flex-end', paddingRight: 20, width: '100%'}} >
                                <TouchableOpacity
                                    onPress={() => {}}
                                >
                                    <Image source={media.heart_white} style={{height: 30, width: 30}} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </TouchableOpacity>
                )}
            />
        </View>
    ) 
}

const styles = StyleSheet.create({
    title: {
        fontSize: 18,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
        marginTop: 14,
        marginBottom: 4,
    },
    cardContainer: {
        height: 240,
        width: 160,
        borderRadius: 10,
        marginRight: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
    },
    iconImage: {
        height: 240,
        width: 160,
        borderRadius: 10,
        resizeMode: 'contain'
    }
})

export default CardContainer