import React from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import { media } from '../../global/media'
import LottieView from 'lottie-react-native'
  
const ScreenLoader = ({  }) => {
    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} >
            <LottieView 
                source={require('../../assets/loader.json')} 
                autoPlay 
                loop 
                style={{flex: 1, height: 100, width: 100}}
            />

        </View>
    )
}

const styles = StyleSheet.create({
    
})

export default ScreenLoader
