import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView, KeyboardAvoidingView,  } from 'react-native'

import { colors } from '../../global/colors'
import Input from '../../components/input'
import { media } from '../../global/media'
import { Poppins } from '../../global/fontFamily'
import LoginButton from '../../components/button/LoginButton'
import { screenHeight } from '../../global/constants'


const WelcomeScreen = ({navigation}) => {

    useEffect(() => {
        
    }, [])




    const loginFunction = () => {
       
    }



    return (
        <View style={styles.container} >
            <Image source={media.logo} style={styles.logo} />
            
    
            <View style={{width: '100%'}} >
                <LoginButton 
                    title="Get Started"
                    onPress={() => {navigation.navigate('Login')}}
                />

                <LoginButton 
                    title="Skip"
                    isOutlined
                    onPress={() => {navigation.navigate('Tabbar')}}
                />
            </View>
        </View> 
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%', 
        alignItems: 'center', 
        justifyContent: 'space-between', 
        flex: 1,  
        padding: 20,
    },
    logo: {
        height: 200,
        width: 200,
        borderRadius: 20,
        marginVertical: 70,
    },
    skip: {
        fontSize: 18,
        fontFamily: Poppins.SemiBold,
        color: colors.primary,
        textDecorationLine: 'underline'
    },
})

export default WelcomeScreen

