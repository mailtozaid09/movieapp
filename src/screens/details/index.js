import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView,  } from 'react-native'

import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { media } from '../../global/media'


import { Rating, AirbnbRating } from 'react-native-ratings';

const baseUrl = 'https://image.tmdb.org/t/p/w500/'

const DetailsScreen = ({navigation, route}) => {

    const [details, setDetails] = useState(route?.params?.details);

    var defaultRating = Math.floor(Math.random() * (5 - 1 + 1)) + 1;

    useEffect(() => {
        //console.log(route?.params?.details);
    }, [])

    return (
        <ScrollView>
            <View style={styles.container} >
                <Image source={{uri: `${baseUrl}${details?.backdrop_path}`}} style={styles.backdropImage} />
                <View style={{position: 'absolute', top: 20, width: '100%'}} >
                    <View style={{ alignItems: 'flex-end', paddingRight: 20, width: '100%'}} >
                        <TouchableOpacity
                            onPress={() => {}}
                        >
                            <Image source={media.heart_fill} style={{height: 30, width: 30}} />
                        </TouchableOpacity>
                    </View>
                </View>
            
                <Text>dsadsadsa</Text>
                <View style={{position: 'absolute', top: 140, width: '100%'}} >
                    <View style={{width: '100%', padding: 20, flexDirection: 'row', alignItems: 'center', }} >
                        <View style={styles.posterPathContainer} >
                            <Image source={{uri: `${baseUrl}${details?.poster_path}`}} style={styles.posterPath} />
                        </View>
                       
                        <View style={{flex: 1, marginLeft: 14, alignItems: 'flex-start'}} >
                            <Text style={styles.title} numberOfLines={3} >{details?.title}</Text>
                            <Text style={styles.title} >{details?.release_date}</Text>
                        </View>
                    </View>
                </View>
                
                <View style={{marginTop: 190, marginBottom: 20, paddingHorizontal: 20, alignItems: 'flex-start'}} >
                    <Text style={[styles.popHeading, {fontSize: 18}]} >Ratings</Text>
                    <AirbnbRating
                        count={5}
                        showRating={false}
                        isDisabled={true}
                        defaultRating={defaultRating}
                        size={30}
                    />
                </View>

                <View style={styles.popularityContainer} >
                    <View>
                        <Text style={styles.popHeading} >{parseInt(details?.vote_average)}</Text>
                        <Text style={styles.popSubHeading} >Vote Average</Text>
                    </View>

                    <View style={styles.divider} />

                    <View>
                        <Text style={styles.popHeading} >{details?.vote_count}</Text>
                        <Text style={styles.popSubHeading} >Vote Count</Text>
                    </View>

                    <View style={styles.divider} />

                    <View>
                        <Text style={styles.popHeading} >{parseInt(details?.popularity)}</Text>
                        <Text style={styles.popSubHeading} >Popularity</Text>
                    </View>
                </View>

                <View style={{padding: 20, paddingTop: 0,}} >
                    <Text style={[styles.popHeading, {fontSize: 18}]} >Overview</Text>
                    <Text style={[styles.popSubHeading, {fontSize: 16, }]} >{details?.overview}</Text>
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    posterPath: {
        height: 220,
        width: 140,
        borderRadius: 10,
        resizeMode: 'contain'
    },
    backdropImage: {
        height: 208,
        width: '100%',
        resizeMode: 'contain',
    },
    posterPathContainer: {
        height: 220,
        width: 140,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
    },
    title: {
        fontSize: 18,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
        marginTop: 2,
    },
    popularityContainer: {
        padding: 20,
        paddingTop: 0,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    popHeading: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    popSubHeading: {
        fontSize: 12,
        fontFamily: Poppins.SemiBold,
        color: '#00000090'
    },
    divider: {
        height: 40,
        width: 1,
        backgroundColor: colors.gray
    }
})

export default DetailsScreen

