import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView,  } from 'react-native'

import { colors } from '../../global/colors'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch, useSelector } from 'react-redux';
import { screenLoader } from '../../store/modules/home/actions';
import ScreenLoader from '../../components/loader/ScreenLoader';
import { Poppins } from '../../global/fontFamily';
import { media } from '../../global/media';
import LoginButton from '../../components/button/LoginButton';
import { currentUserProfile } from '../../store/modules/profile/actions';


const ProfileScreen = ({navigation}) => {

    const dispatch = useDispatch();

    const [userDetails, setUserDetails] = useState({});

    const screen_loader = useSelector(state => state.home.screen_loader);
    // console.log("=screen_loader > ",screen_loader);

    useEffect(() => {
        //dispatch(screenLoader(true))
        getUserDetails()
    }, [])
    

    const getUserDetails = async () => {
        let user = await AsyncStorage.getItem('user_details');  
        let user_details = JSON.parse(user);  
        setUserDetails(user_details)

        console.log("user_details= > ",user_details);
        
        // if(!user_details){
        //     navigation.navigate('LoginStack', {screen: 'Welcome'})
        // }

        // dispatch(screenLoader(false))
    }


    const logoutFunction = () => {
        console.log("logoutFunction => ");

        dispatch(currentUserProfile(null))
        AsyncStorage.clear()

        navigation.navigate('LoginStack', {screen: 'Welcome'})
    }


    const ProfileDetails = () => {
        return(
            <View style={{width: '100%', flex: 1,}} >
                <View style={{height: 120, width: '100%'}} />

                <View style={styles.profileContainer} >
                    <View style={styles.profileImageContainer} >
                        <Image source={media.admin_fill} style={{height: 80, width: 80}} />
                    </View>
                    <View style={{marginTop: 80, width: '100%', alignItems: 'center', justifyContent: 'space-between', flex: 1,}} >
                        <View style={{alignItems: 'center'}} >
                            <Text style={styles.heading} >{userDetails?.userName}</Text>
                            <Text style={styles.subheading} >{userDetails?.email}</Text>
                            <Text style={styles.subheading} >{userDetails?.phoneNumber}</Text>
                        </View>
                        <LoginButton
                            title="Logout"
                            onPress={() => {logoutFunction()}}
                        />
                    </View>
                </View>
            </View>
        )
    }


    const EmptyProfileDetails = () => {
        return(
            <View style={{width: '100%', flex: 1, alignItems: 'center', padding: 20, justifyContent: 'space-between', backgroundColor: colors.white, }} >
                <View style={{alignItems: 'center'}} >
                    <Image source={media.login} style={{height: 200, width: 200, marginTop: 100, marginBottom: 30}} />
                    <Text style={styles.subheading} >Login to continue using app</Text>
                </View>
                <LoginButton
                    title="Login in"
                    onPress={() => {navigation.navigate('LoginStack')}}
                />
            </View>
        )
    }
    return (
        <View style={styles.container} >
            
            {userDetails
            ?
            <ProfileDetails />
            :
            <EmptyProfileDetails />
            }

            {screen_loader && <ScreenLoader />}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        width: '100%',
        backgroundColor: colors.primary,
    },
    profileContainer: {
        backgroundColor: 'white', 
        flex: 1, 
        width: '100%',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: 'center',
    },
    profileImageContainer: {
        position: 'absolute', 
        top: -50, 
        height: 120, 
        width: 120, 
        borderRadius: 60, 
        borderWidth: 1, 
        alignItems: 'center', 
        backgroundColor: colors.white, 
        borderColor: colors.white,
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
    },
    heading: {
        fontSize: 20,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    subheading: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
})

export default ProfileScreen

