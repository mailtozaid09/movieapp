import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView,  } from 'react-native'

import { colors } from '../../global/colors'
import LoginButton from '../../components/button/LoginButton'
import { Poppins } from '../../global/fontFamily'


const FavoriteScreen = ({navigation}) => {

    useEffect(() => {
        
    }, [])

    return (
        <View style={styles.container} >
            <Text style={styles.heading} >Favorite List Empty!!!</Text>
            <LoginButton
                title="Add to Favorites"
                onPress={() => {navigation.navigate('Home')}}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white,
    },
    heading: {
        fontSize: 20,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
})

export default FavoriteScreen

