import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView, KeyboardAvoidingView, Alert,  } from 'react-native'

import Input from '../../components/input'
import LoginButton from '../../components/button/LoginButton'

import { colors } from '../../global/colors'
import { media } from '../../global/media'
import { Poppins } from '../../global/fontFamily'


import { useDispatch, useSelector } from 'react-redux'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { currentUserProfile } from '../../store/modules/profile/actions'


const LoginScreen = ({navigation}) => {

    const dispatch = useDispatch();
     
    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});

    const [showEyeIcon, setShowEyeIcon] = useState(false);

    // const current_user_profile = useSelector(state => state.profile.current_user_profile);
    
    const profile_list = useSelector(state => state.profile.profile_list);

    // console.log("current_user_profile   ", current_user_profile);
    // console.log("profile_list   ", profile_list);

    useEffect(() => {
        
    }, [])


    const onChange = ({ name, value }) => {
        setForm({ ...form, [name]: value });
        setErrors({})
    };


    const loginFunction = () => {
        console.log("form => ", form);

        var isValid = true

        if(!form.userName){
            isValid = false
            setErrors((prev) => {
                return {...prev, userName: 'Please enter a valid username'}
            })
        }

        if(!form.password){
            isValid = false
            setErrors((prev) => {
                return {...prev, password: 'Please enter a valid password'}
            })
        }

        if(isValid){

            var filteredList = profile_list.filter((item) => {
                return item.userName == form.userName && item?.password == form.password
            });


            console.log("filteredList ", filteredList);

            if(filteredList?.length != 0){
                console.log('====================================');
                console.log("login success");
                console.log('====================================');
            
                let userDetails = {  
                    id: filteredList[0].id,
                    userName: filteredList[0].userName,
                    email: filteredList[0].email,
                    phoneNumber: filteredList[0].phoneNumber,
                    password: filteredList[0].password,
                    confirmPassword: filteredList[0].confirmPassword,
                }  
                dispatch(currentUserProfile(userDetails))
                AsyncStorage.setItem('user_details',JSON.stringify(userDetails));  
        
            }else{
                Alert.alert('Account not found!')
                console.log('====================================');
                console.log("login error");
                console.log('====================================');
            }

        }
    }



    return (
        <ScrollView style={{flex: 1,}} >
        <KeyboardAvoidingView  behavior='position' >
            <View style={{width: '100%', alignItems: 'center', padding: 20, }} >
                <Image source={media.logo} style={styles.logo} />
                
                <Text style={styles.heading} >Login to your account</Text>

                <Input
                    label="Username"
                    error={errors.userName}
                    placeholder="Enter your username"
                    onChangeText={(text) => onChange({name: 'userName', value: text})}
                />

                <Input
                    label="Password"
                    error={errors.password}
                    placeholder="Enter your Password"
                    showEyeIcon={showEyeIcon}
                    onChangeEyeIcon={() => setShowEyeIcon(!showEyeIcon)}
                    onChangeText={(text) => onChange({name: 'password', value: text})}
                    isPassword={true}
                />

                <LoginButton 
                    title="Login"
                    onPress={() => {loginFunction()}}
                />

                <View style={{flexDirection: 'row', marginVertical: 10,}} >
                    <Text style={styles.already1} >Don't have an account? </Text>
                    <TouchableOpacity onPress={() => {navigation.navigate('Register')}}>
                        <Text style={styles.already2} >Sign Up</Text>
                    </TouchableOpacity>
                </View>
            </View> 
        </KeyboardAvoidingView>
    </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.white,
    },
    logo: {
        height: 100,
        width: 100,
        borderRadius: 20,
        marginVertical: 20,
    },
    heading: {
        fontSize: 22,
        fontFamily: Poppins.Medium,
        color: colors.black,
        marginBottom: 8,
    },
    already1: {
        fontSize: 14,
        fontFamily: Poppins.Regular,
        color: colors.black,
    },
    already2: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.primary,
        textDecorationLine: 'underline'
    },
})

export default LoginScreen

