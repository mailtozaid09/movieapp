import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView, KeyboardAvoidingView,  } from 'react-native'

import Input from '../../components/input'
import LoginButton from '../../components/button/LoginButton'

import { colors } from '../../global/colors'
import { media } from '../../global/media'
import { Poppins } from '../../global/fontFamily'

import { useDispatch } from 'react-redux'
import { addUserProfile } from '../../store/modules/profile/actions'
import AsyncStorage from '@react-native-async-storage/async-storage'


const RegisterScreen = ({navigation}) => {

    const dispatch = useDispatch();

    const [allUsers, setAllUsers] = useState([]);
    const [showEyeIcon, setShowEyeIcon] = useState(false);
    const [showConfirmEyeIcon, setShowConfirmEyeIcon] = useState(false);
    
    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});


    useEffect(() => {
        
    }, [])

       
    const onChange = ({ name, value }) => {
        setForm({ ...form, [name]: value });
        setErrors({})
    };


    const signUpFunction = () => {
        console.log("form => ", form);

        var isValid = true
        
        if(!form.userName){
            isValid = false
            setErrors((prev) => {
                return {...prev, userName: 'Please enter a valid username'}
            })
        }

        if(!form.email){
            isValid = false
            setErrors((prev) => {
                return {...prev, email: 'Please enter a valid eail'}
            })
        }

    
        if(!form.password){
            isValid = false
            setErrors((prev) => {
                return {...prev, password: 'Please enter a valid password'}
            })
        }

        if(!form.confirmPassword){
            isValid = false
            setErrors((prev) => {
                return {...prev, confirmPassword: "Password doesn't match"}
            })
        }

        if(!form.phoneNumber){
            isValid = false
            setErrors((prev) => {
                return {...prev, phoneNumber: 'Please enter a valid phone number'}
            })
        }

        console.log("isValid => ", isValid);

        if(isValid){

            console.log("saveProfile !!  => ", isValid);
            var userDetails = {
                userName: form.userName,
                email: form.email,
                phoneNumber: form.phoneNumber,
                password: form.password,
                confirmPassword: form.confirmPassword,
            }
            dispatch(addUserProfile(userDetails))
            AsyncStorage.setItem('user_details',JSON.stringify(userDetails));  
            navigation.navigate('Tabbar')
        }
        
    }


    return (
        <ScrollView style={{flex: 1,}} >
                <View style={{flex: 1, width: '100%', alignItems: 'center', marginTop: 10, padding: 20, }} >
                    <Image source={media.logo} style={styles.logo} />
                    
                    <Text style={styles.heading} >Create your account</Text>

                    <Input
                        label="Email"
                        error={errors?.email}
                        placeholder="Enter your email"
                        onChangeText={(text) => onChange({name: 'email', value: text})}
                    />


                    <Input
                        label="Username"
                        error={errors?.userName}
                        placeholder="Enter your username"
                        onChangeText={(text) => onChange({name: 'userName', value: text})}
                    />

                   
                    <Input
                        label="Password"
                        error={errors?.password}
                        placeholder="Enter your Password"
                        showEyeIcon={showEyeIcon}
                        onChangeEyeIcon={() => setShowEyeIcon(!showEyeIcon)}
                        onChangeText={(text) => onChange({name: 'password', value: text})}
                        isPassword={true}
                    />

                    <Input
                        label="Confirm Password"
                        error={errors?.confirmPassword}
                        placeholder="Enter your confirm password"
                        showEyeIcon={showConfirmEyeIcon}
                        onChangeEyeIcon={() => setShowConfirmEyeIcon(!showConfirmEyeIcon)}
                        onChangeText={(text) => onChange({name: 'confirmPassword', value: text})}
                        isPassword={true}
                    />

                    <Input
                        label="Phone Number"
                        error={errors?.phoneNumber}
                        placeholder="Enter your Phone number"
                        keyboradType="numeric"
                        onChangeText={(text) => onChange({name: 'phoneNumber', value: text})}
                    />

                    <LoginButton 
                        title="Sign Up"
                        onPress={() => {signUpFunction()}}
                    />

                    <View style={{flexDirection: 'row', marginVertical: 10,}} >
                        <Text style={styles.already1} >Already have an account? </Text>
                        <TouchableOpacity onPress={() => {navigation.navigate('Login')}}>
                            <Text style={styles.already2} >Sign In</Text>
                        </TouchableOpacity>
                    </View>
                </View> 
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.white,
    },
    logo: {
        height: 100,
        width: 100,
        borderRadius: 20,
        marginVertical: 20,
    },
    heading: {
        fontSize: 22,
        fontFamily: Poppins.Medium,
        color: colors.black,
        marginBottom: 8,
    },
    already1: {
        fontSize: 14,
        fontFamily: Poppins.Regular,
        color: colors.black,
    },
    already2: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.primary,
        textDecorationLine: 'underline'
    },
})

export default RegisterScreen

