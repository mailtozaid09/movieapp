import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView,  } from 'react-native'

import { colors } from '../../global/colors'
import CardContainer from '../../components/custom/CardContainer'
import { getAllMovies, getTrendingMovies } from '../../global/api'


const HomeScreen = ({navigation}) => {

    const [trendingMovies, setTrendingMovies] = useState([]);
    const [watchListMovies, setWatchListMovies] = useState([]);
    const [allMovies, setAllMovies] = useState([]);

    useEffect(() => {
        getTrendingMoviesFunction()
        getAllMoviesFunction()
    }, [])


    const getAllMoviesFunction = () => {
        getAllMovies()
        .then((resp) => {
            //console.log("resp =>> ", resp?.results);
            setAllMovies(resp?.results.reverse())
        })
        .catch((err) => {
            console.log("err =>> ", err);
        })
    }

    const getTrendingMoviesFunction = () => {
        getTrendingMovies()
        .then((resp) => {
            //console.log("resp =>> ", resp?.results);
            setTrendingMovies(resp?.results)
        })
        .catch((err) => {
            console.log("err =>> ", err);
        })
    }

    return (
        <ScrollView>
            <View style={styles.container} >
              
                <CardContainer 
                    title="Trending Movies"
                    data={trendingMovies}
                />

                <CardContainer 
                    title="Watchlist Movies"
                    data={trendingMovies}
                />

                <CardContainer 
                    title="All Movies"
                    data={allMovies}
                />
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        padding: 20,
        backgroundColor: colors.white,
    }
})

export default HomeScreen

