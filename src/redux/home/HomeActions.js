import { 
    ADD_ITEM_TO_CART, REMOVE_ITEM_FROM_CART, 
    ADD_ITEM_TO_SHOPPING_LIST, ADD_SHIPPING_ADDRESS, ADD_TODO_LIST, DELETE_TODO_LIST, SET_PROMO_CODE, UPDATE_ITEM_TO_SHOPPING_LIST, UPDATE_TODO_LIST, EMPTY_CART_LIST, ADD_ITEM_TO_WISHLIST, REMOVE_ITEM_FROM_WISHLIST, 

    SET_USER_DETAILS,
    ADD_USER_TO_LIST,
} from "./HomeActionTypes";

let nextUserId = 0;


export const addUserToList = params => {
    return {
        type: ADD_USER_TO_LIST,
        payload: {
            id: ++nextUserId,
            userName: params.userName,
            email: params.email,
            phoneNumber: params.phoneNumber,
            password: params.password,
            confirmPassword: params.confirmPassword,
        },
    };
};


export const setUserDetails = params => {

    return {
        type: SET_USER_DETAILS,
        payload: {
            id:  params.userName,
            userName: params.userName,
            email: params.email,
            phoneNumber: params.phoneNumber,
            password: params.password,
            confirmPassword: params.confirmPassword,
        },
    };
};


export const addShippingAddress = params => {
    return {
        type: ADD_SHIPPING_ADDRESS,
        payload: {
            id: ++nextUserId,
            fullName: params.fullName,
            address: params.address,
            city: params.city,
            region: params.region,
            zipcode: params.zipcode,
            country: params.country,
        },
    };
};



export const addItemToWishlist = params => {
    return {
        type: ADD_ITEM_TO_WISHLIST,
        payload: {
            id: ++nextUserId,
            image_url: params.image_url,
            category: params.category,
            item_name: params.item_name,
            item_description: params.item_description,
            price: params.price,
            ratings: params.ratings,
        },
    };
};



export const removeItemFromWishlist = item_name => {
    return {
        type: REMOVE_ITEM_FROM_WISHLIST,
        payload: {
            item_name
        },
    };
};




export const addItemToCart = params => {
    return {
        type: ADD_ITEM_TO_CART,
        payload: {
            id: ++nextUserId,
            image_url: params.image_url,
            category: params.category,
            item_name: params.item_name,
            item_description: params.item_description,
            price: params.price,
            ratings: params.ratings,
        },
    };
};



export const removeItemFromCart = item_name => {
    return {
        type: REMOVE_ITEM_FROM_CART,
        payload: {
            item_name
        },
    };
};


export const emptyCartList = () => {
    return {
        type: EMPTY_CART_LIST,
        payload: {
            
        },
    };
};



export const addItemToShoppingList = params => {
    return {
        type: ADD_ITEM_TO_SHOPPING_LIST,
        payload: {
            id: ++nextUserId,
            productName: params.productName,
            productQuantity: params.productQuantity,
            isChecked: params.isChecked,
        },
    };
};


export const updateItemToShoppingList = params => {

    return {
        type: UPDATE_ITEM_TO_SHOPPING_LIST,
        payload: {
            id: params.id,
            productName: params.productName,
            productQuantity: params.productQuantity,
            isChecked: params.isChecked,
        },
    };
};



export const addTodoList = params => {
    return {
        type: ADD_TODO_LIST,
        payload: {
            id: ++nextUserId,
            taskName: params.taskName,
            time: params.time,
            date: params.date,
            category: params.category,
            isCompleted: params.isCompleted,
            isImportant: params.isImportant,
        },
    };
};

export const deleteTodoList = id => {
    return {
        type: DELETE_TODO_LIST,
        payload: {
            id
        },
    };
};


export const updateTodoList = params => {

    return {
        type: UPDATE_TODO_LIST,
        payload: {
            id: params.id,
            taskName: params.taskName,
            time: params.time,
            date: params.date,
            category: params.category,
            isCompleted: params.isCompleted,
            isImportant: params.isImportant,
        },
    };
};
