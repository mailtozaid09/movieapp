import { food_recipes_data } from '../../global/sampleData';
import { 
    ADD_ITEM_TO_CART, REMOVE_ITEM_FROM_CART, EMPTY_CART_LIST,
    
    SET_USER_DETAILS,

    ADD_ITEM_TO_SHOPPING_LIST, ADD_SHIPPING_ADDRESS, ADD_TODO_LIST, DELETE_TODO_LIST, SET_PROMO_CODE, UPDATE_ITEM_TO_SHOPPING_LIST, UPDATE_TODO_LIST, REMOVE_ITEM_FROM_WISHLIST, ADD_ITEM_TO_WISHLIST, ADD_USER_TO_LIST, 
 } from './HomeActionTypes';


const initialState = {
    user_details: {},

    all_user_details: [],
    
    promo_code: '',
    wish_list: [],
    saved_address: [{"address": "9 New Road","number": "9027346976",  "city": "Dehradun", "country": "India", "fullName": "Zaid Ahmed", "id": 1, "region": "Uttrakhand", "zipcode": "248001"}],
    cart_list:  [{"category": "Home", "id": 1, "image_url": "https://m.media-amazon.com/images/I/81f8kWOvmiL._AC_UL800_FMwebp_QL65_.jpg", "item_description": "A soft throw pillow with a geometric pattern.", "item_name": "Throw Pillow", "price": 379, "ratings": 4}],
    
    shopping_list: [],
    todo_list: [],
};


export default HomeReducer = (state = initialState, action) => {
    switch (action.type) {

        case ADD_USER_TO_LIST: {
            const {
                id, userName, email, phoneNumber, password, confirmPassword,
            } = action.payload
            return {
                ...state,
                all_user_details: [
                    ...state.all_user_details, {
                        id, userName, email, phoneNumber, password, confirmPassword,
                    }
                ]
            };
        }


        case SET_USER_DETAILS: {
            const {
                id, userName, email, phoneNumber, password, confirmPassword,
            } = action.payload
                return {
                    ...state,
                    user_details: {
                        id, userName, email, phoneNumber, password, confirmPassword,
                    }
            };
        }
        

        case SET_PROMO_CODE: {
            const { promoCode } = action.payload
                return {
                    ...state,
                    promo_code: promoCode
            };
        }

        
        case ADD_SHIPPING_ADDRESS: {
            const {
                id,
                fullName,
                address,
                city,
                region,
                zipcode,
                country,
            } = action.payload
            return {
                ...state,
                saved_address: [
                    ...state.saved_address, {
                        id,
                        fullName,
                        address,
                        city,
                        region,
                        zipcode,
                        country,
                    }
                ]
            };
        }
        
        
        case ADD_ITEM_TO_WISHLIST: {
            const {
                id, image_url, category, item_name, item_description, price, ratings,
            } = action.payload
            return {
                ...state,
                wish_list: [
                    ...state.wish_list, {
                        id, image_url, category, item_name, item_description, price, ratings,
                    }
                ]
            };
        }

        case REMOVE_ITEM_FROM_WISHLIST: {
            const { item_name } = action.payload
                return {
                    ...state,
                    wish_list: state.wish_list.filter((item) => item.item_name != item_name)
            };
        }



        
        case ADD_ITEM_TO_CART: {
            const {
                id, image_url, category, item_name, item_description, price, ratings,
            } = action.payload
            return {
                ...state,
                cart_list: [
                    ...state.cart_list, {
                        id, image_url, category, item_name, item_description, price, ratings,
                    }
                ]
            };
        }

        case REMOVE_ITEM_FROM_CART: {
            const { item_name } = action.payload
                return {
                    ...state,
                    cart_list: state.cart_list.filter((item) => item.item_name != item_name)
            };
        }

        case EMPTY_CART_LIST: {
            const {  } = action.payload
                return {
                    ...state,
                    cart_list: []
            };
        }





        
        
        case ADD_ITEM_TO_SHOPPING_LIST: {
            const {
                id, 
                productName,
                productQuantity,
                isChecked,
               
            } = action.payload
            return {
                ...state,
                shopping_list: [
                    ...state.shopping_list, {
                        id, 
                        productName,
                        productQuantity,
                        isChecked,
                    }
                ]
            };
        }

        
        case UPDATE_ITEM_TO_SHOPPING_LIST:{
            const { 
                productName,
                productQuantity,
                isChecked, 
            } = action.payload
            const updatedTodo = state.shopping_list.map(todo => {
                if (todo.id != action.payload.id) {
                  return todo;
                } else {
                  return {
                    ...todo,
                        productName: productName,
                        productQuantity: productQuantity,
                        isChecked: isChecked,
                  };
                }
              });
           
            return {
                ...state,
                shopping_list: updatedTodo
            };}
            
            
            
        case ADD_TODO_LIST: {
            const {
                id, 
                title,
                isCompleted,
            } = action.payload
            return {
                ...state,
                todo_list: [
                    ...state.todo_list, {
                        id, 
                        title,
                        isCompleted,
                    }
                ]
            };
        }

        case UPDATE_TODO_LIST:{
            const { title,isCompleted, } = action.payload
            const updatedTodo = state.todo_list.map(todo => {
                if (todo.id != action.payload.id) {
                  return todo;
                } else {
                  return {
                    ...todo,
                    title: title,
                    isCompleted: isCompleted,
                  };
                }
              });
           
            return {
                ...state,
                todo_list: updatedTodo
            };}

        case DELETE_TODO_LIST: {
            const { id } = action.payload
                return {
                    ...state,
                    todo_list: state.todo_list.filter((todo) => todo.id != id)
            };
        }



        default:
            return state;
    }
};
