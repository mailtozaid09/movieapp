import React,{useEffect, useState} from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Image, LogBox, Platform,  } from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { colors } from '../../global/colors';
import { media } from '../../global/media';
import { Poppins } from '../../global/fontFamily';

import HomeStack from '../home';
import ProfileStack from '../profile';
import FavoriteStack from '../favorite';


const Tab = createBottomTabNavigator();


export default function Tabbar({navigation}) {


    const [userDetails, setUserDetails] = useState({});

    useEffect(() => {
        getUserDetails()
    }, [])
    

    const getUserDetails = async () => {
        let user = await AsyncStorage.getItem('user_details');  
        let user_details = JSON.parse(user);  
        setUserDetails(user_details)
    }

    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                let iconName, routeName, height = 22, width = 22 ;
    
                if (route.name === 'HomeStack') {
                    iconName = focused ? media.home_fill : media.home
                    routeName = 'Home' 
                } 
                else if (route.name === 'ProfileStack') {
                    iconName = focused ? media.admin_fill : media.admin
                    routeName = 'Profile' 
                }
                else if (route.name === 'FavoriteStack') {
                    iconName = focused ? media.heart_black : media.heart_white
                    routeName = 'Favorite' 
                }
                return(
                    <View style={[{alignItems: 'center', justifyContent: 'center', height: 44, padding: 12, paddingHorizontal: 15, borderRadius: 22, flexDirection: 'row' }, focused ? {backgroundColor:  colors.white ,} : null ]} >
                        <Image source={iconName} style={{height: height, width: width, marginRight: 10, resizeMode: 'contain'}}/>
                        {focused && <Text style={{fontSize: 14, color: colors.primary, fontFamily: Poppins.Medium}} >{routeName}</Text>}
                    </View>
                );
                },
            })}
        >
            <Tab.Screen
                name="HomeStack"
                component={HomeStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    headerRight: () => null,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'SelectPostType' || routeName === 'AddPostScreen' || routeName === 'SavePostScreen' || routeName === 'ViewPostDetails' || routeName === 'ViewStoryDetails' ) {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />

            {userDetails && 
            (<Tab.Screen
                name="FavoriteStack"
                component={FavoriteStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    headerRight: () => null,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'SelectPostType' || routeName === 'AddPostScreen' || routeName === 'SavePostScreen' || routeName === 'ViewPostDetails' || routeName === 'ViewStoryDetails' ) {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />)}
            
            <Tab.Screen
                name="ProfileStack"
                component={ProfileStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'SelectPostType') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />
        </Tab.Navigator>
    );
}



const styles = StyleSheet.create({
    tabbarStyle: {
        height: Platform.OS == 'ios' ? 90 : 70,
        paddingHorizontal: 12, 
        paddingTop: Platform.OS == 'ios' ? 15 : 0, 
        paddingBottom: Platform.OS == 'android' ? 2 : 30,
        borderTopWidth: 2,
        borderColor: colors.primary,
        backgroundColor: colors.primary
    }
})