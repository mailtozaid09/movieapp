import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../../screens/login';
import RegisterScreen from '../../screens/register';
import WelcomeScreen from '../../screens/welcome';


const Stack = createStackNavigator();


const LoginStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Welcome" 
        >
            <Stack.Screen
                name="Welcome"
                component={WelcomeScreen}
                options={{
                    headerShown: true,
                    // headerLeft: () => null,
                    // headerTitle: 'Cart',
                    // headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                }}
            />
            <Stack.Screen
                name="Login"
                component={LoginScreen}
                options={{
                    headerShown: true,
                    // headerLeft: () => null,
                    // headerTitle: 'Cart',
                    // headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                }}
            />
            <Stack.Screen
                name="Register"
                component={RegisterScreen}
                options={{
                    headerShown: true,
                    // headerLeft: () => null,
                    // headerTitle: 'Cart',
                    // headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                }}
            />
            

        </Stack.Navigator>
    );
}

export default LoginStack