import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../../screens/home';
import DetailsScreen from '../../screens/details';


const Stack = createStackNavigator();


const HomeStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Home" 
        >
            <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    // headerTitle: 'Cart',
                    // headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                }}
            />
            <Stack.Screen
                name="Details"
                component={DetailsScreen}
                options={{
                    headerShown: true,
                    // headerLeft: () => null,
                    // headerTitle: 'Cart',
                    // headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                }}
            />
            

        </Stack.Navigator>
    );
}

export default HomeStack