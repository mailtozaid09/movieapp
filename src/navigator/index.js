import React, {useState, useEffect} from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';


import Tabbar from './tabbar';
import LoginStack from './login';
import { useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { screenHeight } from '../global/constants';



const Stack = createStackNavigator();


const Navigator = ({}) => {

    const navigation = useNavigation()

    const [userDetails, setUserDetails] = useState({});

    // const current_user_profile = useSelector(state => state.profile.current_user_profile);
    // const profile_list = useSelector(state => state.profile.profile_list);

    // console.log("current_user_profile   ", current_user_profile);
    // console.log("profile_list   ", profile_list);
    
    const screen_loader = useSelector(state => state.home.screen_loader);
    console.log("=screen_loader > ",screen_loader);

    useEffect(() => {
        getUserDetails()
    }, [])
    

    const getUserDetails = async () => {
        let user = await AsyncStorage.getItem('user_details');  
        let user_details = JSON.parse(user);  
        setUserDetails(user_details)

        if(user_details){
            navigation.navigate('Tabbar')
        }

        console.log('====================================');
        console.log("navigator user_details=  ", user_details);
        console.log('====================================');
    }
   


    return (
        <>
        <Stack.Navigator 
        initialRouteName='LoginStack'    
        >
            <Stack.Screen
                name="LoginStack"
                component={LoginStack}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="Tabbar"
                component={Tabbar}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
        
        </>
    );
}

export default Navigator