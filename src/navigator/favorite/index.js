import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../../screens/home';
import DetailsScreen from '../../screens/details';
import FavoriteScreen from '../../screens/favorite';


const Stack = createStackNavigator();


const FavoriteStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Favorite" 
        >
            <Stack.Screen
                name="Favorite"
                component={FavoriteScreen}
                options={{
                    headerShown: true,
                    // headerLeft: () => null,
                    // headerTitle: 'Cart',
                    // headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                }}
            />
        </Stack.Navigator>
    );
}

export default FavoriteStack