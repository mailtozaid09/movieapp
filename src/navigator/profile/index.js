import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import ProfileScreen from '../../screens/profile';


const Stack = createStackNavigator();


const ProfileStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Profile" 
        >
            <Stack.Screen
                name="Profile"
                component={ProfileScreen}
                options={{
                    headerShown: true,
                    // headerLeft: () => null,
                    // headerTitle: 'Cart',
                    // headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                }}
            />

        </Stack.Navigator>
    );
}

export default ProfileStack