const INITIAL_STATE = {
  current_user_profile: null,
  profile_list: [],
};

export default function profile(state = INITIAL_STATE, action) {
    switch (action.type) {

        case '@profile/CURRENT_USER_PROFILE':
        return {
            ...state,
            current_user_profile: action.payload.current_user_profile,
        };

        case '@profile/ADD_USER_PROFILE': {
            const { 
                id, userName, email, phoneNumber, password, confirmPassword,
            } = action.payload

            return {
                ...state,
                profile_list: [
                    ...state.profile_list, { 
                        id, userName, email, phoneNumber, password, confirmPassword,
                    }
                ]
            };
        }

        case '@profile/UPDATE_USER_PROFILE': {
            const { 
                id,
                user_name,
                user_icon,
            } = action.payload

            const updatedProfileList = state.profile_list.map(profile => {
                if (profile.id != action.payload.id) {
                  return profile;
                } else {
                  return {
                    ...profile, 
                    user_name,
                    user_icon,
                  };
                }
              });
           
            return {
                ...state,
                profile_list: updatedProfileList
            };}

        case '@profile/DELETE_USER_PROFILE': {
            const { id } = action.payload
                return {
                    ...state,
                    profile_list: state.profile_list.filter((profile) => profile.id != id)
            };
        }


        default:
        return state;
    }
}
