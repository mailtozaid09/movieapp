import { Alert } from 'react-native';
import { users } from '../../../global/sampleData';
import AsyncStorage from '@react-native-async-storage/async-storage';

export function screenLoader(screen_loader) {
  return {
    type: '@home/SCREEN_LOADER',
    payload: {
      screen_loader,
    },
  };
}
