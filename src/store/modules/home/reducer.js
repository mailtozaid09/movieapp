const INITIAL_STATE = {
  screen_loader: false
};

export default function home(state = INITIAL_STATE, action) {
  switch (action.type) {

      case '@home/SCREEN_LOADER':
      return {
        ...state,
        screen_loader: action.payload.screen_loader,
    };
    default:
      return state;
  }
}
