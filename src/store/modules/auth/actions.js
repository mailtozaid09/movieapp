import { Alert } from 'react-native';
import { users } from '../../../global/sampleData';
import AsyncStorage from '@react-native-async-storage/async-storage';

let nextUserId = 0;

export function signIn(email, password) {
  const user = users.filter(
    u => u.email === email && u.password === password
  )[0];

  if (user) {
      console.log("user succes=> " , user);
       AsyncStorage.setItem("user_details", JSON.stringify(user));
  }else{
    Alert.alert('Warning', 'Incorrect email or password');
  }

  return {
    type: '@auth/SIGN_IN',
    payload: {
      user,
    },
  };
}

export function signOut() {
  return {
    type: '@auth/SIGN_OUT',
  };
}

